const { Users, Cars } = require("../models");

class usersRepository {
  static async create({ name, email, password, role }) {
    const createdUser = Users.create({
      name: name,
      email: email,
      password: password,
      role: role,
    });
    return createdUser;
  }

  static async getByEmail({email}) {
    const getUser = await Users.findOne({where: {email: email}});
    return getUser;
  }
}

module.exports = usersRepository;
